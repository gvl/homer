"""


"""
import re
import os
import sys
import subprocess
import optparse
import shutil
import tempfile

def getFileString(fpath, outpath):
    """
    format a nice file size string
    """
    size = ''
    fp = os.path.join(outpath, fpath)
    s = '? ?'
    if os.path.isfile(fp):
        n = float(os.path.getsize(fp))
        if n > 2**20:
            size = ' (%1.1f MB)' % (n/2**20)
        elif n > 2**10:
            size = ' (%1.1f KB)' % (n/2**10)
        elif n > 0:
            size = ' (%d B)' % (int(n))
        s = '%s %s' % (fpath, size) 
    return s

class makeTagDirectory():
    """wrapper
    """

    def __init__(self,opts=None, args=None):
        self.opts = opts
        self.args = args
        
    def run_makeTagDirectory(self):
        """
        makeTagDirectory <Output Directory Name> [options] <alignment file1> [alignment file 2] 

        """
        if self.opts.format != "bam":
            cl = [self.opts.executable] + args + ["-format" , self.opts.format]
        else:
            cl = [self.opts.executable] + args
        print cl
        p = subprocess.Popen(cl)
        retval = p.wait()


        html = self.gen_html(args[0])
        #html = self.gen_html()
        return html,retval

    def gen_html(self, dr=os.getcwd()):
        flist = os.listdir(dr)
        print flist
        """ add a list of all files in the tagdirectory
        """
        res = ['<div class="module"><h2>Files created by makeTagDirectory</h2><table cellspacing="2" cellpadding="2">\n']

        flist.sort()
        for i,f in enumerate(flist):
             if not(os.path.isdir(f)):
                 fn = os.path.split(f)[-1]
                 res.append('<tr><td><a href="%s">%s</a></td></tr>\n' % (fn,getFileString(fn, dr)))

        res.append('</table>\n') 

        return res

if __name__ == '__main__':
    op = optparse.OptionParser()
    op.add_option('-e', '--executable', default='makeTagDirectory')
    op.add_option('-o', '--htmloutput', default=None)
    op.add_option('-f', '--format', default="sam")
    opts, args = op.parse_args()
    #assert os.path.isfile(opts.executable),'## makeTagDirectory.py error - cannot find executable %s' % opts.executable

    #if not os.path.exists(opts.outputdir): 
        #os.makedirs(opts.outputdir)
    f = makeTagDirectory(opts, args)

    html,retval = f.run_makeTagDirectory()
    f = open(opts.htmloutput, 'w')
    f.write(''.join(html))
    f.close()
    if retval <> 0:
         print >> sys.stderr, serr # indicate failure
    


